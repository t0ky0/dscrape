#!/bin/bash
if [[ $# -eq 0 ]] ; then
	echo 'Usage: <option> <www.example.com>'
	echo 'Options: -u Output list of scraped urls'
	echo '         -a Output list of host addresses'
	exit 0
fi
html=$(wget -qO- $2)
result=$(echo $html | grep -o 'http://[^"]*' | cut -d "/" -f 3 | sort -u)
while getopts ":au" opt; do
	case ${opt} in
	a)
          for url in $(echo $result); do host $url; done | grep "has address" | cut -d " " -f1-4 | sort -u
	  ;;
	u)
	  echo "$result"
	  ;;
        esac
done
